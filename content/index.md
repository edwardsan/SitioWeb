---
date: 2016-03-08T21:07:13+01:00
title: Golang Venezuela
type: index
weight: 0
---

## Bienvenido

Somos la primera comunidad de desarrolladores en el lenguaje de programación [Golang](https://golang.org) de Venezuela. Siguenos y participa en nuestros [repositorios publicos](https://github.com/GolangVE) y comparte con nosotros en nuestras distintas redes sociales y demás medios de comunicación. Hacemos importantes aportes, foros, conferencias y participaciones del tema entre miembros y comunidades Golang en todo el mundo.

![Material Screenshot](/images/screen.png)

Contamos con el primer sitio web totalmente en este lenguaje de entre todas las comunidades de las americas, con ayuda del framework [Hugo](https://gohugo.io) y haciendo uso del hospedaje que brindan las plataformas [GitHub](https://github.com/GolangVE) y [GitLab](https://gitlab.com/GolangVE) de las cuales podrás hacer uso libre de nuestras librerias y [repositorios publicos](https://github.com/GolangVE).

## Conocenos

El proyecto **Golang Venezuela** nace de la iniciativa del programador y desarrollador venezolano [Junior Carrillo](http://juniorcarrillo.com.ve) con la idea principal de ser el punto de partida para el parendizaje de este importante lenguaje de programación de alto nivel.

A mediados del año 2015 con el nacimiento de la comunidad [Golang Dominicana](https://facebook.com/groups/golangdominicana) y posteriormente [Golang en Español](https://facebook.com/groups/golang-es), se adiere al grupo **Golang Venezuela** con el firme proposito de como las anteriores, dar a conocer y desarrollar el mayor conocimiento posible de dicho lenguaje de proramación en el territorio venezolano y latinoamericano.

## Objetivos

- Difución y divulgación del lenguaje en todo el territorio nacional. Por medio de charlas, conferecias, conversatorios, foros y talleres en distintos lugares de interes en la nación lograr la mayor penetración del conocimiento sobre este fabuloso lenguaje de programación en el territorio nacional, con el patrocionio de universidades y centros de educación superior e intermedio.

- Educar, certificar y aportar la mayor cantidad conocimientos con la mejor calidad posible y talento venezolano. Amparados en las leyes y degretos de divulgación tecnologícas del país y en acuerdo con las licencias de uso en cada desarrollo informático utilizado. 

- Compartir e intercambiar a todos los niveles los conocimientos adquiridos en todas los ambitos de uso que ecistan o puedan generarse por medio de dicho lenguaje de programación respetando los derechos de autor y la libertad del conocimiento humano el cual se plantea precerbar para la posteridad.

- Alvergar a la mayor cantidad de programadores y desarrolladores del lenguaje que sea posible, para la expansión del mismo sin dejar a un lado los que se sientan afines a otros lenguajes pero que hagan uso pasajero de **Golang** en proyectos profesionales o para terceros.

Golang Venezuela es la primera comunidad de desarrollador del proyecto [Golang](https://golang.org) en el pais, pero esperamos no ser la única.

## Miembros

Nuestra comunidad de programadores y desarrolladores actualmente cuenta con un grupo de miembros no solo de Venezuela, sino de distintas latitudes, los cuales además son miembros de varias comunidades a la ves:

- [Junior Carrillo](http://juniorcarrillo.com.ve) [Fundador] / Guárico, Venezuela
- [Phanor Coll](https://ve.linkedin.com/in/phanorcoll) / Merida, Venezuela
- [Oswaldo Montaño](http://oswaldom.com.ve) / Guárico, Venezuela
- [Alejandro Arnaud Peralta](https://do.linkedin.com/in/alejandro-arnaud-peralta-9590a4a8) / Santo Domingo, República Dominicana
- [Alexys Lozada](https://www.facebook.com/AlexysLozada?fref=nf) / Bogota, Colombia

Recibir una [certificación](/certificaciones/) como desarrollador de Golang en Venezuela es muy facíl, solo participa en nuestros eventos y has notar tu presencia en nuestras distintas plataformas.